import com.typesafe.startscript.StartScriptPlugin

name := "cutthecrap"

version := "0.1-SNAPSHOT"

organization := "Xebia"

scalaVersion := "2.10.0"

scalacOptions := Seq("-encoding", "utf8",
                     "-target:jvm-1.6",
                     "-feature",
                     "-language:implicitConversions",
                     "-language:postfixOps",
                     "-unchecked",
                     "-deprecation",
                     "-Xlog-reflective-calls",
                     "-Ywarn-adapted-args"
                    )

mainClass in Global := Some("com.xebia.xsd.ctc.Main")

// ============================================================================
// Dependencies
// ============================================================================

resolvers ++= Seq("Sonatype Releases"   at "http://oss.sonatype.org/content/repositories/releases",
                  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
                  "Spray Repository"    at "http://repo.spray.io/",
                  "Spray Nightlies"     at "http://nightlies.spray.io/")

libraryDependencies ++= {
  val akkaVersion  = "2.1.0"
  val sprayVersion = "1.1-20130123"
  Seq(
    "com.typesafe.akka"       %%  "akka-actor"         % akkaVersion,
    "com.typesafe.akka"       %%  "akka-slf4j"         % akkaVersion,
    "io.spray"                %   "spray-routing"      % sprayVersion,
    "io.spray"                %   "spray-can"          % sprayVersion,
    "io.spray"                %   "spray-client"       % sprayVersion,
    "io.spray"                %%  "spray-json"         % "1.2.3",
    "ch.qos.logback"          %   "logback-classic"    % "1.0.9",
    "com.typesafe.akka"       %%  "akka-testkit"       % akkaVersion    % "test",
    "io.spray"                %   "spray-testkit"      % sprayVersion   % "test",
    "org.specs2"              %%  "specs2"             % "1.13"         % "test",
    "org.mockito"             %   "mockito-all"        % "1.9.5"        % "test",
    "org.scalatest"           %%  "scalatest"          % "1.9.1"        % "test"
  )
}


// ============================================================================
// Plugin Settings
// ============================================================================

seq(StartScriptPlugin.startScriptForClassesSettings: _*)
