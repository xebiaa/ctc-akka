package com.xebia.xsd.ctc

import akka.actor._
import SupervisorStrategy._

import spray.routing._
import spray.http._

import akka.actor.ActorKilledException
import akka.actor.OneForOneStrategy

class ApiActor extends Actor with HttpService {

  implicit def actorRefFactory: ActorRefFactory = context.system

  def receive = runRoute(routes)

  val routes = {
    // Debugging: /ping -> pong
    path("ping") {
      get {
        complete("pong")
      }
    }
  }

  // ==========================================================================
  //
  // ==========================================================================

  override val supervisorStrategy = OneForOneStrategy() {
    case _: ActorInitializationException => Escalate // if any actor cannot be created, we should fail the entire system
    case _: ActorKilledException         => Restart
    case _: Exception                    => Restart
    case _                               => Escalate
  }
}
