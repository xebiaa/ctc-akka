package com.xebia.xsd.ctc

import akka.actor.Props
import spray.can.server.SprayCanHttpServerApp


object Main extends App with SprayCanHttpServerApp {
  val api = system.actorOf(Props(new ApiActor()), "api-actor")

  newHttpServer(api) ! Bind(interface = "0.0.0.0", port = Settings.Http.Port)
}
