package com.xebia.xsd.ctc

import com.typesafe.config.ConfigFactory
import spray.util.ConfigUtils._


object Settings extends {
  val config = ConfigFactory.load()

  // ==========================================================================
  // Http API
  // ==========================================================================

  object Http {
    private val c = prepareSubConfig(config, "ctc.http")

    val Port = c getInt "port"
  }
}
